<?php

namespace Ztech\Payments\Plugin\Observer;

use Magento\Framework\Registry;
use Magento\Vault\Model\PaymentTokenManagement;
use Magento\Customer\Model\Session;

/**
 * Class PayflowProAddCcDataPlugin
 * @package Ztech\Payments\Plugin\Observer
 */
class PayflowProAddCcDataPlugin
{

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Vault\Model\PaymentTokenManagement
     */
    protected $paymentTokenManagement;


    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * PayflowProAddCcDataPlugin constructor.
     * @param \Magento\Vault\Model\PaymentTokenManagement $paymentTokenManagement
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\Registry $registry
     */
    public function __construct(
        PaymentTokenManagement $paymentTokenManagement,
        Session $customerSession,
        Registry $registry
    ) {
        $this->paymentTokenManagement = $paymentTokenManagement;
        $this->customerSession = $customerSession;
        $this->registry = $registry;
    }

    /**
     * @param \Magento\Paypal\Observer\PayflowProAddCcData $subject
     * @param callable $proceed
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function aroundExecute(
        \Magento\Paypal\Observer\PayflowProAddCcData $subject,
        callable $proceed,
        \Magento\Framework\Event\Observer $observer
    ) {
        $paymentModel = $observer->getData('payment_model');
        $additionalInformation = $paymentModel->getAdditionalInformation();
        $cvv = $additionalInformation['cvv2match'];
        $this->registry->register('cvv', $cvv, true);
    }
}