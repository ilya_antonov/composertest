<?php
namespace Ztech\Payments\Plugin;

use Magento\Checkout\Model\Session;

class QuotePaymentPlugin
{
    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * Construct
     *
     * @param Session $checkoutSession
     */
    public function __construct(
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }
   
    /**
     * 
     * @param \Magento\Quote\Model\Quote\Payment $subject
     * @param array $additionalData
     * @return array
     */
    public function beforeSetAdditionalData(
        \Magento\Quote\Model\Quote\Payment $subject,
        $additionalData
    ) {
        if (isset($additionalData['cc_number'])) {
            $this->checkoutSession->setCybersourceCcNumber($additionalData['cc_number']);
        }
    }
}