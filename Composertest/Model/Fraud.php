<?php
namespace Ztech\Payments\Model;

use Magento\Framework\Model\AbstractModel;

class Fraud extends AbstractModel
{
    /**
     * Constants
     */
    const FRAUD = 1;
    const NOT_FRAUD = 0;
    
    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Ztech\Payments\Model\ResourceModel\Fraud');
    }
}

