<?php

namespace Ztech\Payments\Model\ResourceModel\Order\Grid;

use Ztech\Payments\Setup\InstallSchema;
use Magento\Sales\Model\ResourceModel\Order\Grid\Collection as OriginalCollection;

class Collection extends OriginalCollection
{

    /**
     * @return $this|\Magento\Sales\Model\ResourceModel\Order\Grid\Collection|void
     */
    protected function _initSelect()
    {
        parent::_initSelect();

        $this->getSelect()->joinLeft(
            ['so' => $this->getTable(InstallSchema::TABLE)],
            'main_table.entity_id = so.entity_id',
            ['fraud']
        );

        return $this;
    }
}
