<?php
namespace Ztech\Payments\Ui\Component\Listing\Column;

use Ztech\Payments\Model\Fraud as FraudModel;

class Fraud extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * Prepare Data Source
     * 
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource) {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (!isset($item['fraud'])) {
                    $item['fraud'] = FraudModel::NOT_FRAUD;
                }
            }
        }

        return $dataSource;
    }
}