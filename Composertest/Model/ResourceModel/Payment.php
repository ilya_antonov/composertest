<?php

namespace Ztech\Payments\Model\ResourceModel;

/**
 * Class Payment
 * @package Ztech\Payments\Model\ResourceModel
 */
class Payment extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init(
            'cybersource_payment_token',
            'cybersource_payment_token_id'
        );
    }
}
