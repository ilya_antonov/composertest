<?php
namespace Ztech\Payments\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Ztech\Payments\Setup\InstallSchema;

class Fraud extends AbstractDb 
{
    /**
     * Primary key auto increment flag
     *
     * @var bool
     */
    protected $_isPkAutoIncrement = false;
    
    /**
     * Constructor
     * 
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }

    /**
     * Constructor
     */
    protected function _construct() {
        $this->_init(InstallSchema::TABLE, 'entity_id');
    }
}
