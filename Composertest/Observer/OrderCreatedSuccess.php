<?php

namespace Ztech\Payments\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Ztech\Payments\Model\FraudFactory;
use Magento\Sales\Model\Order;
use Ztech\Payments\Model\Fraud;

/**
 * Class OrderPlaceBefore
 */
class OrderCreatedSuccess implements ObserverInterface
{
    /**
     * @var FraudFactory
     */
    protected $fraudFactory;

    /**
     * Construct
     *
     * @param FraudFactory $fraudFactory
     */
    public function __construct(
        FraudFactory $fraudFactory
    ) {
        $this->fraudFactory = $fraudFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getEvent()->getOrder();
        $orderId = $order->getId();
        $isFraud = $order->getStatus() == (string) Order::STATUS_FRAUD;
        $fraudFactory = $this->fraudFactory;
        try {
            /** @var Ztech\Payments\Model\FraudFactory $factoryObject */
            $fraudFactory->create()
                ->setData('entity_id', $orderId)
                ->setData('fraud', $this->getFraudLabel($isFraud))
                ->save();
        } catch (Exception $e) {
            /** @todo Add message to logger **/
        }
    }

    /**
     * @param $isFraud
     * @return string
     */
    public function getFraudLabel($isFraud)
    {
        return ($isFraud ? Fraud::FRAUD : Fraud::NOT_FRAUD);
    }
}