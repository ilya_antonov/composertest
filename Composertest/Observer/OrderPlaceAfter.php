<?php 

namespace Ztech\Payments\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use CyberSource\SecureAcceptance\Service\CyberSourceSoapApi as Client;
use Magento\Sales\Model\Order;
use Magento\Checkout\Model\Session;
use CyberSource\SecureAcceptance\Gateway\Validator\SoapReasonCodeValidator;

/**
 * Class OrderPlaceAfter
 */
class OrderPlaceAfter implements ObserverInterface
{
    /**
     * @var \CyberSource\SecureAcceptance\Service\CyberSourceSoapApi
     */
    protected $client;
    
    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * OrderPlaceAfter constructor.
     * 
     * @param Client $client
     * @param Session $checkoutSession
     */
    public function __construct(
        Client $client,
        Session $checkoutSession
    ) {
        $this->client = $client;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @throws \Exception
     */
    public function execute(Observer $observer)
    {
        $order = $observer->getOrder();
        $payment = $order->getPayment();
        try {
            $isFraud = $this->checkFraud($order);
            if ($isFraud) {
                $order->setState(Order::STATE_PROCESSING)->setStatus(Order::STATUS_FRAUD);
                $payment->setIsFraudDetected($isFraud);
            } else {
                $order->setState(Order::STATE_PROCESSING);
            }
        } catch (Exception $e) {
            /** @todo Add message to logger **/
        }
    }

    /**
     * @param $order
     * @return bool
     * @throws \Exception
     */
    protected function checkFraud($order)
    {
        $request = $this->buildRequest($order);
        $response = $this->client->run($request);
        $reasonCode = $response->ccAuthReply->reasonCode;

        return SoapReasonCodeValidator::DM_REVIEW == $reasonCode;
    }

    /**
     * Build request
     *
     * @param Order $order
     * @return array
     */
    protected function buildRequest($order)
    {
        $payment = $order->getPayment();
        $request = [];
        $request['ccAuthService'] = [
            'run' => 'true'
        ];

        $request['merchantReferenceCode'] = $order->getIncrementId();

        $request['billTo'] = [
            'firstName' => $order->getBillingAddress()->getFirstname(),
            'lastName' => $order->getBillingAddress()->getLastname(),
            'email' => $order->getBillingAddress()->getEmail(),
            'country' => $order->getBillingAddress()->getCountryId(),
            'city' => $order->getBillingAddress()->getCity(),
            'street1' => $order->getBillingAddress()->getStreet()[0],
            'state'  => $order->getBillingAddress()->getRegion(),
            'postalCode' => $order->getBillingAddress()->getPostcode()
        ];
        
        $ccNumber = $this->checkoutSession->getCybersourceCcNumber();
        $this->checkoutSession->unsetCybersourceCcNumber();
        
        $request['card'] = [
            'accountNumber' => $ccNumber,
            'expirationMonth' => $payment->getCcExpMonth(),
            'expirationYear' => $payment->getCcExpYear()
        ];
        
        /** @todo get card number **/
        $request['purchaseTotals']['currency'] = $order->getStoreCurrencyCode();

        $products = $order->getAllVisibleItems();
        foreach ($products as $key => $product) {
            if ($price = $product->getPrice()) {
                $request['item'][$key]['unitPrice'] = $price;
            }
        }
        $request = (object) $request;

        return $request;
    }
}