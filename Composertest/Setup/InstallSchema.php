<?php

namespace Ztech\Payments\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Db\Ddl\Table;

/**
 * Class InstallSchema
 * @package Ztech\Payments\Setup
 */
class InstallSchema implements InstallSchemaInterface
{

    const TABLE = 'canon_fraud';

    /**
     * @param \Magento\Framework\Setup\SchemaSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tableName = $setup->getTable(self::TABLE);

        if ($setup->getConnection()->isTableExists($tableName) != true) {
            $table = $setup->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'entity_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ]
                )->addColumn(
                    'fraud',
                    Table::TYPE_TEXT,
                    32,
                    [
                        'nullable' => false,
                        'unique' => true,
                        'unsigned' => true,
                        'default' => 0
                    ]
                )->addIndex(
                    $setup->getIdxName(self::TABLE, ['fraud']),
                    ['fraud']
                )
                ->setComment('FRAUD')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');

            $setup->getConnection()->createTable($table);
        }

        $setup->endSetup();
    }
}
