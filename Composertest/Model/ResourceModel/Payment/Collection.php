<?php

namespace Ztech\Payments\Model\ResourceModel\Payment;

/**
 * Class Collection
 * @package Ztech\Payments\Model\ResourceModel\Payment
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'cybersource_payment_token_id';


    protected function _construct()
    {
        $this->_init(
            'Ztech\Payments\Model\Payment',
            'Ztech\Payments\Model\ResourceModel\Payment'
        );
    }
}
